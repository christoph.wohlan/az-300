# az-300

## Einführung

[Kurs Übersicht](www.az.training)
[Azure Portal](http://portal.azure.com)

[Azure Sponsorships - Kostenübersicht](https://www.microsoftazuresponsorships.com/Balance)

[Git Repo](https://github.com/MicrosoftLearning/AZ-300-MicrosoftAzureArchitectTechnologies)
[Azure Portal](https://azure.microsoft.com/de-de/features/azure-portal/)

user christoph.wohlan@gmail.com Ca
Dann oben rechts auf "Portal" klicken
Für **Cloud Shell** oben auf das Symbol klicken und dann unten New Session

[Azure Quickstart Templates](https://github.com/Azure/azure-quickstart-templates)

## az

    az login

    az resource list -o table
    Name                      ResourceGroup                   Location    Type                               Status
    ------------------------  ------------------------------  ----------  ---------------------------------  --------
    csbd289828a1fbbx4cd8xbaf  cloud-shell-storage-westeurope  westeurope  Microsoft.Storage/storageAccounts
  
## wir bauen eine VM im Portal
Rsource Grouprg-test01
VM Name cwn-ubuntu01
chwohlan
ssh
Virtual network rg-test01-vnet
Hostname 40.118.27.8
User chwohlan

Ubuntu mit NGinX

Hashicorp
Tool: packer
config.file
template03.json

## Build Image with packer

Note: To list Azure regions, run az account list-locations --output table
RG=$(az group create --name az3000301-LabRG --location "westeurope")
AAD_SP=$(az ad sp create-for-rbac)

...


sed -i.bak1 's/"$CLIENT_ID"/"'"$CLIENT_ID"'"/' ~/template03.json
christoph@Azure:~$ sed -i.bak2 's/"$CLIENT_SECRET"/"'"$CLIENT_SECRET"'"/' ~/template03.json
christoph@Azure:~$ sed -i.bak3 's/"$TENANT_ID"/"'"$TENANT_ID"'"/' ~/template03.json
christoph@Azure:~$ sed -i.bak4 's/"$SUBSCRIPTION_ID"/"'"$SUBSCRIPTION_ID"'"/' ~/template03.json
christoph@Azure:~$ sed -i.bak5 's/"$LOCATION"/"'"$LOCATION"'"/' ~/template03.json
christoph@Azure:~$ cat template03.json
{
    "builders": [{
      "type": "azure-arm",

      "client_id": "6d633dc9-ff7d-4c8c-bd2b-300f3430c7b1",
      "client_secret": "9ba1bd0b-95f4-48d4-9ed6-5cb662ab8b6d",
      "tenant_id": "8fc12ed4-6655-41be-85a5-61f55b40d6bd",
      "subscription_id": "d289828a-1fbb-4cd8-bafb-18f1441fa221",

      "managed_image_resource_group_name": "az3000301-LabRG",
      "managed_image_name": "az3000301-image",

...

az vm create --resource-group az3000301-LabRG --name az3000301-vm --image az3000301-image --admin-username student --generate-ssh-keys

az vm open-port --resource-group az3000301-LabRG --name az3000301-vm --port 80

az network public-ip show --resource-group az3000301-LabRG --name az3000301-vmPublicIP --query ipAddress

https://github.com/MicrosoftLearning/AZ-300-MicrosoftAzureArchitectTechnologies/tree/master/Instructions




## Tag 2

    PS Azure:\> New-AzResourceGroup -Name az3000601-LabRG -Location "westeurope"

    ResourceGroupName : az3000601-LabRG
    Location          : westeurope
    ProvisioningState : Succeeded
    Tags              :
    ResourceId        : /subscriptions/d289828a-1fbb-4cd8-bafb-18f1441fa221/resourceGroups/az3000601-LabRG



## Monitoring
Azure Monitor
Log Analytics  Abfragesprache "Kusto"
Application insides
Network Watcher
Action Group

[Übung](https://github.com/Azure/azure-quickstart-templates/tree/master/201-vmss-bottle-autoscale)

Test-AzDnsAvailability -DomainNameLabel "cwnstest1" -Location "westeurope"


## Storage

Azure Blobs  (mit SAS Shared Access Signatures) Mehr: Data Lake
Azure Files (SMB Samba)
Azure Tables
Azure Queues

[Datei-, Block- oder Objekt-Storage?](https://www.redhat.com/de/topics/data-storage/file-block-object-storage)

[SAN vs NAS](https://www.netapp.com/de/info/what-is-storage-area-network.aspx)

AzCopy
Azure Speicher Explorer

[Schnellstart: Verwalten von Blobs per JavaScript v12 SDK in Node.js](https://docs.microsoft.com/de-de/azure/storage/blobs/storage-quickstart-blobs-nodejs)


net use fileshare mit Katzenvideo

https://cwnstorageaccount.file.core.windows.net/videos/

Identity-based Directory Service for Azure File Authentication

SMB access

$connectTestResult = Test-NetConnection -ComputerName cwnstorageaccount.file.core.windows.net -Port 445
if ($connectTestResult.TcpTestSucceeded) {
    # Save the password so the drive will persist on reboot
    cmd.exe /C "cmdkey /add:`"cwnstorageaccount.file.core.windows.net`" /user:`"Azure\cwnstorageaccount`" /pass:`"ZXsqRJ9xdWOGv1lQgdSG91b[...]q3VHIrD/fSqokdfOlaIxI/O58J2fo6kg==`""
    # Mount the drive
    New-PSDrive -Name Z -PSProvider FileSystem -Root "\\cwnstorageaccount.file.core.windows.net\videos"-Persist
} else {
    Write-Error -Message "Unable to reach the Azure storage account via port 445. Check to make sure your organization or ISP is not blocking port 445, or use Azure P2S VPN, Azure S2S VPN, or Express Route to tunnel SMB traffic over a different port."
}

## K8s

https://azure.microsoft.com/de-de/pricing/details/kubernetes-service/
https://azure.microsoft.com/de-de/overview/kubernetes-getting-started/


## Netzwerke

bgp border gateway protocol

az group create --resource-group az3000401-LabRG --location "westeurope"
az group create --resource-group az3000402-LabRG --location "westeurope"

az group deployment create --resource-group az3000401-LabRG --template-file azuredeploy0401.json --parameters azuredeploy04.parameters.json --no-wait

az group deployment create --resource-group az3000402-LabRG --template-file azuredeploy0402.json --parameters azuredeploy04.parameters.json --no-wait

VNet1                VNet 2
|--VM1               |--VM2
|   10.0.0.4         |   10.0.4.4
|                    |
|--VM2               |
|   10.0.1.4         |
|                    |
|-----peering--------|


Tip: Windows Firewall ausschalten
Appliance (NVA)  installieren RRAS - Rout Table mit rule nötig
Peering einrichten
Übung: ping oder testnetconnection

ping 10.0.0.4
ping
tracert -d 10.0.0.4

Route: 
name: custom-route-to-az3000401-vnet
prefix: 10.0.0.0/22
next hop: 10.0.1.4

auf VNetz2 - VM1 (10.0.4.4) mit und ohne route in VNetz2 Subnet1

    C:\Users\Student>ipconfig

    Windows IP Configuration

    Ethernet adapter Ethernet:

    Connection-specific DNS Suffix  . : k0bhaxcg5xnerap0ng1jbh4gza.ax.internal.cloudapp.net
    Link-local IPv6 Address . . . . . : fe80::953a:4f6d:a5c2:bb67%5
    IPv4 Address. . . . . . . . . . . : 10.0.4.4
    Subnet Mask . . . . . . . . . . . : 255.255.255.0
    Default Gateway . . . . . . . . . : 10.0.4.1
    [...]

    C:\Users\Student>tracert -d 10.0.0.4

    Tracing route to 10.0.0.4 over a maximum of 30 hops

    1     2 ms     1 ms     1 ms  10.0.0.4

    Trace complete.

    C:\Users\Student>tracert -d 10.0.0.4

    Tracing route to 10.0.0.4 over a maximum of 30 hops

    1     2 ms    <1 ms    <1 ms  10.0.1.4
    2     2 ms     2 ms     2 ms  10.0.0.4

    Trace complete.

Vebinden per Bastion (subnet ) AzureBastionSubnet
GatewaySubnet


## Gateway

see point-tosite-vpn.azcli

    # Exercise 1 Create Virtual Gateway in the Hub VNet
    # =================================================
    # Tasks
    #      0 Verify hub and spoke
    #      1 Create gateway subnet
    #      2 Create gateway public ip
    #      3 Create gateway
    #      4 Configure "Allow gateway transit" on hub
    #      5 Configure "Use remote gateway" on spoke
    #      6 Configure "Virtual network gateway route propagation"

[Hub and Spoke](# http://www.differencebetween.net/technology/hardware-technology/difference-between-a-hub-a-spoke-and-a-point-to-point/)

### Certificat

    PS C:\Users\Teilnehmer> $rootCert = New-SelfSignedCertificate `
    >>               -Type Custom `
    >>               -KeySpec Signature `
    >>               -Subject 'CN=AdatumRootCertificate' `
    >>               -KeyExportPolicy Exportable `
    >>               -HashAlgorithm sha256 `
    >>               -KeyLength 2048 `
    >>               -CertStoreLocation 'Cert:\CurrentUser\My' `
    >>               -KeyUsageProperty Sign `
    >>               -KeyUsage CertSign `
    >>               -FriendlyName 'AdatumRootCertificate'

    PS C:\Users\Teilnehmer> [System.Convert]::ToBase64String($rootCert.RawData) | clip

    PS C:\Users\Teilnehmer> New-SelfSignedCertificate `
    >>   -Type Custom `
    >>   -KeySpec Signature `
    >>   -Subject 'CN=AdatumClientCertificate' `
    >>   -KeyExportPolicy Exportable `
    >>   -HashAlgorithm sha256 `
    >>   -KeyLength 2048 `
    >>   -CertStoreLocation 'Cert:\CurrentUser\My' `
    >>   -Signer $rootCert `
    >>   -TextExtension @("2.5.29.37={text}1.3.6.1.5.5.7.3.2") `
    >>   -FriendlyName 'AdatumClientCertificate'

    PSParentPath: Microsoft.PowerShell.Security\Certificate::CurrentUser\My

    Thumbprint                                Subject
    ----------                                -------
    F258FFF962B9DCB86F70E9D151990D0E664E78A7  CN=AdatumClientCertificate

    PS C:\Users\Teilnehmer> Get-ChildItem Cert:\CurrentUser\My

    PSParentPath: Microsoft.PowerShell.Security\Certificate::CurrentUser\My

    Thumbprint                                Subject
    ----------                                -------
    F258FFF962B9DCB86F70E9D151990D0E664E78A7  CN=AdatumClientCertificate
    95AA01C44C12AF2BCF8F8618D643BD574B03A496  CN=AdatumRootCertificate

    PS C:\Users\Teilnehmer>

Im Azure Portal den VPN Client (Config) herunterladen und installieren (AMD64)

In den Windows Einstellungen - Netzwerk - VPN - verbinden

    Microsoft Windows [Version 10.0.17763.1039]
    (c) 2018 Microsoft Corporation. Alle Rechte vorbehalten.

    C:\Users\Teilnehmer>ping 10.0.0.4

    Ping wird ausgeführt für 10.0.0.4 mit 32 Bytes Daten:
    Zeitüberschreitung der Anforderung.
    Antwort von 10.0.0.4: Bytes=32 Zeit=19ms TTL=127
    Antwort von 10.0.0.4: Bytes=32 Zeit=19ms TTL=127
    Antwort von 10.0.0.4: Bytes=32 Zeit=19ms TTL=127

    Ping-Statistik für 10.0.0.4:
        Pakete: Gesendet = 4, Empfangen = 3, Verloren = 1
        (25% Verlust),
    Ca. Zeitangaben in Millisek.:
        Minimum = 19ms, Maximum = 19ms, Mittelwert = 19ms

    C:\Users\Teilnehmer>tracert -d 10.0.4.4

    Routenverfolgung zu 10.0.4.4 über maximal 30 Hops

    1    17 ms    17 ms    18 ms  192.168.0.1
    2    19 ms    19 ms    19 ms  10.0.4.4

    Ablaufverfolgung beendet.

Oder eine RDP Verbindung auf den Windows Server 10.0.0.4 (Student/Pa55w.rd1234) starten 


## Channel 9

channel9.msdn.com

## Loadbalancer

VNet

Installation des IIS per Desired State Configuration (DSC) auf beiden Windows VMs
Port 80, Round Robin auf jeweils IS
Inbount NAT Rule 33890 auf VM0 3389 und 33891 auf VM1 3389 und 
Innerhalb deselben VNEt
          LB

      :3389       :3389
       VM0         VM1
     10.0.0.4    10.0.0.5
         |           |
         |           |
        ----------------

### Application Gateway 

User Name: Student
Password: Pa55w.rd


az group create --name az3000801-LabRG --location westeurope


Windows PowerShell
Copyright (C) 2016 Microsoft Corporation. All rights reserved.

PS C:\Users\Student> Invoke-RestMethod http://ipinfo.io/json
ip       : 51.124.70.88                           -> 51.124.70.88 (az3000802-lb-pip01)
city     : Amsterdam
region   : North Holland
country  : NL
loc      : 52.3618,4.9280
org      : AS8075 Microsoft Corporation
postal   : 1093
timezone : Europe/Amsterdam
readme   : https://ipinfo.io/missingauth


### Create Azure Active Directory (Azure AD Free)

## einen Tenant anlegen

Aus dem Portal
wohlansoft.onmicrosoft.com  Country: Germany

-> [wohlansoft - Übersicht](https://portal.azure.com/#blade/Microsoft_AAD_IAM/ActiveDirectoryMenuBlade/Overview)

Jetzt nur noch eine Subscription anlegen und schon gehts.



az group create --resource-group az3000501-LabRG --location westeurope


Label:
Test-AzDnsAvailability -DomainNameLabel cwn-test -Location westeurope
True

DSC Konfiguration


## Übung RBAC

New-AzResourceGroup -Name az3000901-LabRG -Location westeurope

(Get-AzureADUser -Filter "MailNickName eq 'labuser0901'").UserPrincipalName

labuser0901@christophwohlangmail.onmicrosoft.com

    Fehler beim Neustart des virtuellen Computers "az3000901-vm". Fehler: Der Client "labuser0901@christophwohlangmail.onmicrosoft.com" mit der Objekt-ID ed155bbd-8a21-4744-9dc4-eff83017f684 verfügt über keine Autorisierung zum Ausführen der Aktion "Microsoft.Compute/virtualMachines/restart/action" über den Bereich "az3000901-LabRG/providers/Microsoft.Compute/virtualMachines/az3000901-vm'>az3000901-vm", oder der Bereich ist ungültig. 

    cwn-wordpress.azurewebsites.net

    https://cwn-wordpress.azurewebsites.net
    FTP Hostname: ftp://waws-prod-am2-295.ftp.azurewebsites.windows.net


## WebApps
SourceCode GitHub
Build Server "kudu"
LOCATION='westeurope'
az group create --name $RESOURCE_GROUP --location $LOCATION


RESOURCE_GROUP='AADesignLab0402-RG'
LOCATION='westeurope'
az group create --name $RESOURCE_GROUP --location $LOCATION
az aks create --resource-group $RESOURCE_GROUP --name aad0402-akscluster --node-count 1 --node-vm-size Standard_DS1_v2 --generate-ssh-keys


Connect to the AKS cluster.

az aks get-credentials --resource-group $RESOURCE_GROUP --name cwnk8s
At the Cloud Shell command prompt, type in the following command and press Enter to verify connectivity to the AKS cluster:

kubectl get nodes
At the Cloud Shell command prompt, review the output and verify that the node is reporting the Ready status. Rerun the command until the correct status is shown.

    christoph@Azure:~$ RESOURCE_GROUP='AADesignLab0402-RG'
    christoph@Azure:~$ LOCATION='westeurope'
    christoph@Azure:~$ az aks get-credentials --resource-group $RESOURCE_GROUP --name cwnk8s
    Merged "cwnk8s" as current context in /home/christoph/.kube/config
    christoph@Azure:~$ kubectl get nodes
    NAME                                STATUS   ROLES   AGE   VERSION
    aks-agentpool-28823374-vmss000000   Ready    agent   58m   v1.14.8
    aks-agentpool-28823374-vmss000001   Ready    agent   58m   v1.14.8
    aks-agentpool-28823374-vmss000002   Ready    agent   58m   v1.14.8
    christoph@Azure:~$


    christoph@Azure:~$ kubectl describe node
    Name:               aks-agentpool-28823374-vmss000000
    Roles:              agent
    Labels:             agentpool=agentpool
                        beta.kubernetes.io/arch=amd64
                        beta.kubernetes.io/instance-type=Standard_B2s
                        beta.kubernetes.io/os=linux
                        failure-domain.beta.kubernetes.io/region=westeurope
                        failure-domain.beta.kubernetes.io/zone=0
                        kubernetes.azure.com/cluster=MC_AADesignLab0402-RG_cwnk8s_westeurope
                        kubernetes.azure.com/role=agent
                        kubernetes.io/arch=amd64
                        kubernetes.io/hostname=aks-agentpool-28823374-vmss000000
                        kubernetes.io/os=linux
                        kubernetes.io/role=agent
                        node-role.kubernetes.io/agent=
                        storageprofile=managed
                        storagetier=Premium_LRS
    Annotations:        node.alpha.kubernetes.io/ttl: 0
                        volumes.kubernetes.io/controller-managed-attach-detach: true
    CreationTimestamp:  Fri, 28 Feb 2020 12:10:10 +0000
    Taints:             <none>
    Unschedulable:      false
    Conditions:
    Type                 Status  LastHeartbeatTime                 LastTransitionTime                Reason                       Message
    ----                 ------  -----------------                 ------------------                ------                       -------
    NetworkUnavailable   False   Fri, 28 Feb 2020 12:11:07 +0000   Fri, 28 Feb 2020 12:11:07 +0000   RouteCreated                 RouteController created a route
    MemoryPressure       False   Fri, 28 Feb 2020 13:10:51 +0000   Fri, 28 Feb 2020 12:10:10 +0000   KubeletHasSufficientMemory   kubelet has sufficient memory available
    DiskPressure         False   Fri, 28 Feb 2020 13:10:51 +0000   Fri, 28 Feb 2020 12:10:10 +0000   KubeletHasNoDiskPressure     kubelet has no disk pressure
    PIDPressure          False   Fri, 28 Feb 2020 13:10:51 +0000   Fri, 28 Feb 2020 12:10:10 +0000   KubeletHasSufficientPID      kubelet has sufficient PID available
    Ready                True    Fri, 28 Feb 2020 13:10:51 +0000   Fri, 28 Feb 2020 12:10:10 +0000   KubeletReady                 kubelet is posting ready status. AppArmor enabled
    Addresses:
    Hostname:    aks-agentpool-28823374-vmss000000
    InternalIP:  10.240.0.4
    Capacity:
    attachable-volumes-azure-disk:  4
    cpu:                            2
    ephemeral-storage:              101445900Ki
    hugepages-1Gi:                  0
    hugepages-2Mi:                  0
    memory:                         4017260Ki
    pods:                           110
    Allocatable:
    attachable-volumes-azure-disk:  4
    cpu:                            1900m
    ephemeral-storage:              93492541286
    hugepages-1Gi:                  0
    hugepages-2Mi:                  0
    memory:                         2200684Ki
    pods:                           110
    System Info:
    Machine ID:                 a428923fe1af448e92c96d1170d4ab3d
    System UUID:                35A716A5-5CEA-6E4D-9C47-440D3495DF05
    Boot ID:                    82144ed8-84c5-4b3b-8ee1-71c88ec87ead
    Kernel Version:             4.15.0-1069-azure
    OS Image:                   Ubuntu 16.04.6 LTS
    Operating System:           linux
    Architecture:               amd64
    Container Runtime Version:  docker://3.0.8
    Kubelet Version:            v1.14.8
    Kube-Proxy Version:         v1.14.8
    PodCIDR:                     10.244.0.0/24
    ProviderID:                  azure:///subscriptions/d289828a-1fbb-4cd8-bafb-18f1441fa221/resourceGroups/mc_aadesignlab0402-rg_cwnk8s_westeurope/providers/Microsoft.Compute/virtualMachineScaleSets/aks-agentpool-28823374-vmss/virtualMachines/0
    Non-terminated Pods:         (8 in total)
    Namespace                  Name                                    CPU Requests  CPU Limits  Memory Requests  Memory Limits  AGE
    ---------                  ----                                    ------------  ----------  ---------------  -------------  ---
    kube-system                coredns-6c66fc4fcb-7cbkx                100m (5%)     0 (0%)      70Mi (3%)        170Mi (7%)     64m
    kube-system                coredns-autoscaler-567dc76d66-dlzlv     20m (1%)      0 (0%)      10Mi (0%)        0 (0%)         64m
    kube-system                kube-proxy-g9llq                        100m (5%)     0 (0%)      0 (0%)           0 (0%)         61m
    kube-system                kubernetes-dashboard-9f5bf9974-t7ftj    100m (5%)     100m (5%)   50Mi (2%)        500Mi (23%)    64m
    kube-system                metrics-server-5695787788-z49mv         0 (0%)        0 (0%)      0 (0%)           0 (0%)         64m
    kube-system                omsagent-m8cph                          75m (3%)      150m (7%)   225Mi (10%)      600Mi (27%)    61m
    kube-system                omsagent-rs-745688bcd4-hwj4n            150m (7%)     1 (52%)     250Mi (11%)      750Mi (34%)    64m
    kube-system                tunnelfront-cb7669d7f-74ssx             10m (0%)      0 (0%)      64Mi (2%)        0 (0%)         64m
    Allocated resources:
    (Total limits may be over 100 percent, i.e., overcommitted.)
    Resource                       Requests     Limits
    --------                       --------     ------
    cpu                            555m (29%)   1250m (65%)
    memory                         669Mi (31%)  2020Mi (93%)
    ephemeral-storage              0 (0%)       0 (0%)
    attachable-volumes-azure-disk  0            0
    Events:                          <none>


    Name:               aks-agentpool-28823374-vmss000001
    Roles:              agent
    Labels:             agentpool=agentpool
                        beta.kubernetes.io/arch=amd64
                        beta.kubernetes.io/instance-type=Standard_B2s
                        beta.kubernetes.io/os=linux
                        failure-domain.beta.kubernetes.io/region=westeurope
                        failure-domain.beta.kubernetes.io/zone=1
                        kubernetes.azure.com/cluster=MC_AADesignLab0402-RG_cwnk8s_westeurope
                        kubernetes.azure.com/role=agent
                        kubernetes.io/arch=amd64
                        kubernetes.io/hostname=aks-agentpool-28823374-vmss000001
                        kubernetes.io/os=linux
                        kubernetes.io/role=agent
                        node-role.kubernetes.io/agent=
                        storageprofile=managed
                        storagetier=Premium_LRS
    Annotations:        node.alpha.kubernetes.io/ttl: 0
                        volumes.kubernetes.io/controller-managed-attach-detach: true
    CreationTimestamp:  Fri, 28 Feb 2020 12:10:13 +0000
    Taints:             <none>
    Unschedulable:      false
    Conditions:
    Type                 Status  LastHeartbeatTime                 LastTransitionTime                Reason                       Message
    ----                 ------  -----------------                 ------------------                ------                       -------
    NetworkUnavailable   False   Fri, 28 Feb 2020 12:11:17 +0000   Fri, 28 Feb 2020 12:11:17 +0000   RouteCreated                 RouteController created a route
    MemoryPressure       False   Fri, 28 Feb 2020 13:10:54 +0000   Fri, 28 Feb 2020 12:10:13 +0000   KubeletHasSufficientMemory   kubelet has sufficient memory available
    DiskPressure         False   Fri, 28 Feb 2020 13:10:54 +0000   Fri, 28 Feb 2020 12:10:13 +0000   KubeletHasNoDiskPressure     kubelet has no disk pressure
    PIDPressure          False   Fri, 28 Feb 2020 13:10:54 +0000   Fri, 28 Feb 2020 12:10:13 +0000   KubeletHasSufficientPID      kubelet has sufficient PID available
    Ready                True    Fri, 28 Feb 2020 13:10:54 +0000   Fri, 28 Feb 2020 12:10:13 +0000   KubeletReady                 kubelet is posting ready status. AppArmor enabled
    Addresses:
    Hostname:    aks-agentpool-28823374-vmss000001
    InternalIP:  10.240.0.5
    Capacity:
    attachable-volumes-azure-disk:  4
    cpu:                            2
    ephemeral-storage:              101445900Ki
    hugepages-1Gi:                  0
    hugepages-2Mi:                  0
    memory:                         4017260Ki
    pods:                           110
    Allocatable:
    attachable-volumes-azure-disk:  4
    cpu:                            1900m
    ephemeral-storage:              93492541286
    hugepages-1Gi:                  0
    hugepages-2Mi:                  0
    memory:                         2200684Ki
    pods:                           110
    System Info:
    Machine ID:                 efdf84ccf3f44d73b75403c693f0cc65
    System UUID:                C799B69F-30C1-6143-8C5E-B05A21009790
    Boot ID:                    105db4f1-f72b-47ee-87fd-c31a66dcc97c
    Kernel Version:             4.15.0-1069-azure
    OS Image:                   Ubuntu 16.04.6 LTS
    Operating System:           linux
    Architecture:               amd64
    Container Runtime Version:  docker://3.0.8
    Kubelet Version:            v1.14.8
    Kube-Proxy Version:         v1.14.8
    PodCIDR:                     10.244.1.0/24
    ProviderID:                  azure:///subscriptions/d289828a-1fbb-4cd8-bafb-18f1441fa221/resourceGroups/mc_aadesignlab0402-rg_cwnk8s_westeurope/providers/Microsoft.Compute/virtualMachineScaleSets/aks-agentpool-28823374-vmss/virtualMachines/1
    Non-terminated Pods:         (3 in total)
    Namespace                  Name                        CPU Requests  CPU Limits  Memory Requests  Memory Limits  AGE
    ---------                  ----                        ------------  ----------  ---------------  -------------  ---
    kube-system                coredns-6c66fc4fcb-qrsb8    100m (5%)     0 (0%)      70Mi (3%)        170Mi (7%)     61m
    kube-system                kube-proxy-xqgm4            100m (5%)     0 (0%)      0 (0%)           0 (0%)         61m
    kube-system                omsagent-rkktg              75m (3%)      150m (7%)   225Mi (10%)      600Mi (27%)    61m
    Allocated resources:
    (Total limits may be over 100 percent, i.e., overcommitted.)
    Resource                       Requests     Limits
    --------                       --------     ------
    cpu                            275m (14%)   150m (7%)
    memory                         295Mi (13%)  770Mi (35%)
    ephemeral-storage              0 (0%)       0 (0%)
    attachable-volumes-azure-disk  0            0
    Events:
    Type     Reason               Age   From                                           Message
    ----     ------               ----  ----                                           -------
    Normal   Starting             60m   kube-proxy, aks-agentpool-28823374-vmss000001  Starting kube-proxy.
    Warning  FailedToCreateRoute  60m   route_controller                               Could not create route 460d0a20-5a23-11ea-abaf-2e820b4ab0cc 10.244.1.0/24 for node aks-agentpool-28823374-vmss000001 after 125.426633ms: timed out waiting for the condition
    Warning  FailedToCreateRoute  60m   route_controller                               Could not create route 460d0a20-5a23-11ea-abaf-2e820b4ab0cc 10.244.1.0/24 for node aks-agentpool-28823374-vmss000001 after 5.108205853s: timed out waiting for the condition
    Warning  FailedToCreateRoute  60m   route_controller                               Could not create route 460d0a20-5a23-11ea-abaf-2e820b4ab0cc 10.244.1.0/24 for node aks-agentpool-28823374-vmss000001 after 115.711207ms: timed out waiting for the condition


    Name:               aks-agentpool-28823374-vmss000002
    Roles:              agent
    Labels:             agentpool=agentpool
                        beta.kubernetes.io/arch=amd64
                        beta.kubernetes.io/instance-type=Standard_B2s
                        beta.kubernetes.io/os=linux
                        failure-domain.beta.kubernetes.io/region=westeurope
                        failure-domain.beta.kubernetes.io/zone=2
                        kubernetes.azure.com/cluster=MC_AADesignLab0402-RG_cwnk8s_westeurope
                        kubernetes.azure.com/role=agent
                        kubernetes.io/arch=amd64
                        kubernetes.io/hostname=aks-agentpool-28823374-vmss000002
                        kubernetes.io/os=linux
                        kubernetes.io/role=agent
                        node-role.kubernetes.io/agent=
                        storageprofile=managed
                        storagetier=Premium_LRS
    Annotations:        node.alpha.kubernetes.io/ttl: 0
                        volumes.kubernetes.io/controller-managed-attach-detach: true
    CreationTimestamp:  Fri, 28 Feb 2020 12:10:18 +0000
    Taints:             <none>
    Unschedulable:      false
    Conditions:
    Type                 Status  LastHeartbeatTime                 LastTransitionTime                Reason                       Message
    ----                 ------  -----------------                 ------------------                ------                       -------
    NetworkUnavailable   False   Fri, 28 Feb 2020 12:11:17 +0000   Fri, 28 Feb 2020 12:11:17 +0000   RouteCreated                 RouteController created a route
    MemoryPressure       False   Fri, 28 Feb 2020 13:10:58 +0000   Fri, 28 Feb 2020 12:10:18 +0000   KubeletHasSufficientMemory   kubelet has sufficient memory available
    DiskPressure         False   Fri, 28 Feb 2020 13:10:58 +0000   Fri, 28 Feb 2020 12:10:18 +0000   KubeletHasNoDiskPressure     kubelet has no disk pressure
    PIDPressure          False   Fri, 28 Feb 2020 13:10:58 +0000   Fri, 28 Feb 2020 12:10:18 +0000   KubeletHasSufficientPID      kubelet has sufficient PID available
    Ready                True    Fri, 28 Feb 2020 13:10:58 +0000   Fri, 28 Feb 2020 12:10:18 +0000   KubeletReady                 kubelet is posting ready status. AppArmor enabled
    Addresses:
    Hostname:    aks-agentpool-28823374-vmss000002
    InternalIP:  10.240.0.6
    Capacity:
    attachable-volumes-azure-disk:  4
    cpu:                            2
    ephemeral-storage:              101445900Ki
    hugepages-1Gi:                  0
    hugepages-2Mi:                  0
    memory:                         4017256Ki
    pods:                           110
    Allocatable:
    attachable-volumes-azure-disk:  4
    cpu:                            1900m
    ephemeral-storage:              93492541286
    hugepages-1Gi:                  0
    hugepages-2Mi:                  0
    memory:                         2200680Ki
    pods:                           110
    System Info:
    Machine ID:                 a666badc4c1b40c5b721ba6e67729cfc
    System UUID:                D49EEFCA-49B4-0F44-8BEE-D7BBDE566A6B
    Boot ID:                    c489be86-591e-4843-96a1-3f455bcccc20
    Kernel Version:             4.15.0-1069-azure
    OS Image:                   Ubuntu 16.04.6 LTS
    Operating System:           linux
    Architecture:               amd64
    Container Runtime Version:  docker://3.0.8
    Kubelet Version:            v1.14.8
    Kube-Proxy Version:         v1.14.8
    PodCIDR:                     10.244.2.0/24
    ProviderID:                  azure:///subscriptions/d289828a-1fbb-4cd8-bafb-18f1441fa221/resourceGroups/mc_aadesignlab0402-rg_cwnk8s_westeurope/providers/Microsoft.Compute/virtualMachineScaleSets/aks-agentpool-28823374-vmss/virtualMachines/2
    Non-terminated Pods:         (2 in total)
    Namespace                  Name                CPU Requests  CPU Limits  Memory Requests  Memory Limits  AGE
    ---------                  ----                ------------  ----------  ---------------  -------------  ---
    kube-system                kube-proxy-8qjm8    100m (5%)     0 (0%)      0 (0%)           0 (0%)         61m
    kube-system                omsagent-lq9r9      75m (3%)      150m (7%)   225Mi (10%)      600Mi (27%)    61m
    Allocated resources:
    (Total limits may be over 100 percent, i.e., overcommitted.)
    Resource                       Requests     Limits
    --------                       --------     ------
    cpu                            175m (9%)    150m (7%)
    memory                         225Mi (10%)  600Mi (27%)
    ephemeral-storage              0 (0%)       0 (0%)
    attachable-volumes-azure-disk  0            0
    Events:                          <none>
    christoph@Azure:~$


    christoph@Azure:~$ kubectl run cwnk8s --image=nginx --replicas=1 --port=80
    kubectl run --generator=deployment/apps.v1 is DEPRECATED and will be removed in a future version. Use kubectl run --generator=run-pod/v1 or kubectl create instead.
    deployment.apps/cwnk8s created
    
    christoph@Azure:~$ kubectl get deployment
    NAME     READY   UP-TO-DATE   AVAILABLE   AGE
    cwnk8s   1/1     1            1           48s
    
    christoph@Azure:~$ kubectl get pods
    NAME                      READY   STATUS    RESTARTS   AGE
    cwnk8s-54dcb4dbc7-r54jz   1/1     Running   0          60s
    
    christoph@Azure:~$ kubectl expose deployment cwnk8s --port=80 --type=LoadBalancer
    service/cwnk8s exposed
    
    christoph@Azure:~$ kubectl get service --watch
    NAME         TYPE           CLUSTER-IP   EXTERNAL-IP   PORT(S)        AGE
    cwnk8s       LoadBalancer   10.0.64.40   <pending>     80:30172/TCP   22s
    kubernetes   ClusterIP      10.0.0.1     <none>        443/TCP        69m
    ...
    christoph@Azure:~$ kubectl get service --watch
    NAME         TYPE           CLUSTER-IP   EXTERNAL-IP      PORT(S)        AGE
    cwnk8s       LoadBalancer   10.0.64.40   51.105.234.202   80:30172/TCP   107s
    kubernetes   ClusterIP      10.0.0.1     <none>           443/TCP        70m


    ### Voting App

    git clone https://github.com/Azure-Samples/azure-voting-app-redis.git
    cd azure-voting-app-redis

https://docs.bitnami.com/azure/get-started-aks/


## Dashboard

az aks browse --resource-group 'AADesignLab0402-RG' --name 'cwnk8s'

## Functions


christoph@Azure:~$ export PREFIX=$(echo `openssl rand -base64 5 | cut -c1-7 | tr '[:upper:]' '[:lower:]' | tr -cd '[[:alnum:]]._-'`)
christoph@Azure:~$ export LOCATION='westeurope'
christoph@Azure:~$ export RESOURCE_GROUP_NAME='az300T0602-LabRG'
christoph@Azure:~$ az group create --name "${RESOURCE_GROUP_NAME}" --location "$LOCATION"
{
  "id": "/subscriptions/d289828a-1fbb-4cd8-bafb-18f1441fa221/resourceGroups/az300T0602-LabRG",
  "location": "westeurope",
  "managedBy": null,
  "name": "az300T0602-LabRG",
  "properties": {
    "provisioningState": "Succeeded"
  },
  "tags": null,
  "type": "Microsoft.Resources/resourceGroups"
}
christoph@Azure:~$


az functionapp config appsettings set --name "${FUNCTION_NAME}" --resource-group "${RESOURCE_GROUP_NAME}" --settings "APPINSIGHTS_INSTRUMENTATIONKEY=$APPINSIGHTS_KEY" FUNCTIONS_EXTENSION_VERSION=~2